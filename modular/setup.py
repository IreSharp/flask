from setuptools import setup

setup(
    name='modular',
    packages=['modular'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
