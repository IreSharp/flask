from json import dumps
from modular import app, helpers
from flask import request, jsonify, url_for, redirect
from .db.models import *
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

@app.route('/')
def home():
    return 'Base System'

@app.route('/book/<int:id>', strict_slashes=False)
def returnBook(name=None,id=None):
        
    queryResult = Book.query.get_or_404(id)

    result = BookSchema(False).dump(queryResult)
    return jsonify(result.data)

@app.route('/books')
def returnAllBooks():

    values = Book.query.all()

    return jsonify(
        helpers.paginetValues(
            values,
            BookSchema,
            '/books', 
            start=int(request.args.get('start') if request.args.get('start') else 1),
            limit=int(request.args.get('limit') if request.args.get('limit') else 10)
        )
    )

@app.route('/book/new', methods=['POST'])
def createBook():

    if request.form['name'] and request.form['name'] != ' ':
        
        bookTemp = Book(
            name=request.form['name'],
            gender=request.form['gender']
        )

        db.session.add(bookTemp)
        db.session.flush()
        db.session.commit()
    
        response = app.response_class(
            response='book created',
            status=201,
            mimetype='application/text',
            headers={
                'Location': url_for('returnBook', id=bookTemp.id)
            }
        )
    else:
        response = app.response_class(
            response='Name field is mandadory',
            status=400,
            mimetype='application/text'
        )

    return response

@app.route('/book/tes')
def re():
    
    bookTemp = db.session.query(Book).get(request.form['id'])
    
    if bookTemp is None:
        return app.response_class(
            response='Object not found',
            status=404,
            mimetype='application/text'
        )

    result = BookSchema(False).dump(bookTemp)
    return jsonify(result.data)

@app.route('/book/update', methods=['PUT'])
def updateBook():
    
    bookTemp = Book.query.get_or_404(request.form['id'])
    
    bookTemp.name = request.form['name'] if request.form['name'] else bookTemp.name
    bookTemp.gender = request.form['gender'] if request.form['gender'] else bookTemp.gender

    db.session.merge(bookTemp)
    db.session.commit()

    return redirect(
        url_for(
            'returnBook', 
            id=request.form['id']
        )
    )

