from modular import app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import ModelSchema

db = SQLAlchemy(app)
ma = Marshmallow(app)


class Book(db.Model):#TODO adicionar id_autor
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), unique = True, nullable = False)
    gender = db.Column(db.String(40), nullable = False)

    def __init__(self, name, gender):
        self.name = name
        self.gender = gender

    def __str__(self):
        return f'name = {self.name}, gender = {self.gender}'
    
class BookSchema(ma.ModelSchema):
    class Meta:
        model = Book

# class Author(db.Model):
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     name = db.Column(db.String(80), unique = True, nullable = False)
#     books = db.relationship('Book', secondary = 'books', lazy = 'subquery',
#         brackref = db.backref('author', lazy = True))

# class AutorSchema(ma.ModelSchema):
#     class Meta:
#         model = Autor

db.create_all()

# gen = 0
# for i in range(60):
#     if i > 14:
#         if i % 15 == 0:
#             gen += 1
#     db.session.add(
#         Book(
#             name=f'Book {i}',
#             gender=f'Gender {gen}'
#         )
#     )
# db.session.commit()