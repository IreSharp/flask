from modular import app
from .db.models import Book, BookSchema



# def queryHandler(klass, schema, url, start, limit, query=None):

#     if query:
#         results = klass.query.filter(query[:-4]).all()
#     else:
#         results = klass.query.all()

#     return paginetValues(result, schema, url, start, limit):

def paginetValues(values, schema, url, start, limit):
    # check if page exists

    count = len(values)
    if (count < start):
        abort(404)
    # make response
    obj = {}
    obj['start'] = start
    obj['limit'] = limit
    obj['count'] = count
    # make URLs
    # make previous url
    if start == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)
    # make next url
    if start + limit > count:
        obj['next'] = ''
    else:
        start_copy = start + limit
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)
    # finally extract result according to bounds
    # obj['results'] = results[(start - 1):(start - 1 + limit)]
    obj['results'] = schema(many=True).dump(values[(start - 1):(start - 1 + limit)]).data
    return obj

# def returnPaginatedValues(klass, schema, query, url, start, limit):
#     # check if page exists

#     results = klass.query.filter(query[:-4]).all()

#     count = len(results)
#     if (count < start):
#         abort(404)
#     # make response
#     obj = {}
#     obj['start'] = start
#     obj['limit'] = limit
#     obj['count'] = count
#     # make URLs
#     # make previous url
#     if start == 1:
#         obj['previous'] = ''
#     else:
#         start_copy = max(1, start - limit)
#         limit_copy = start - 1
#         obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)
#     # make next url
#     if start + limit > count:
#         obj['next'] = ''
#     else:
#         start_copy = start + limit
#         obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)
#     # finally extract result according to bounds
#     # obj['results'] = results[(start - 1):(start - 1 + limit)]
#     obj['results'] = schema(many=True).dump(results[(start - 1):(start - 1 + limit)]).data
#     return obj