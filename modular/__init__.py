from flask import Flask

app = Flask(__name__)
# app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///modular.db"

import modular.views
from modular.db.models import Book, BookSchema